<?php
/* Fungsi untuk melakukan autentikasi*/
function do_login($username, $password)
{
    /* Daftar akun mahasiswa yang diizinkan login (gantikan dengan data akun yang sesuai)*/
    $allowed_accounts = array(
        'rahma' => '296',
    );

    /* Periksa apakah akun yang diinputkan sesuai dengan yang diizinkan*/
    if (isset($allowed_accounts[$username]) && $allowed_accounts[$username] === $password) {
        return true;
    }
    return false;
}

/* Cek apakah ada data form yang di-submit*/
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    /* Lakukan validasi */
    if (do_login($username, $password)) {
        /* validasi berhasil */
        header("Location: selamat_datang.php");
        exit;
    } else {
        /* validasi gagal */
        header("Location: login.php?error=1");
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Mahasiswa</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-image: url("bg.gif");
            margin: 0;
            padding: 0;
        }

        h1 {
            text-align: center;
            color: whitesmoke;
        }

        form {
            max-width: 300px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        label {
            display: block;
            margin-bottom: 5px;
            color: #333;
        }

        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            box-sizing: border-box;
        }

        input[type="submit"] {
            width: 100%;
            padding: 10px;
            background-color: #333;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #444;
        }

        p.error {
            color: red;
            font-size: 14px;
            text-align: center;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <center>
        <br><br><br><br><br><br>
    <h1>Login Mahasiswa</h1>

    <!-- Tampilkan pesan error jika ada -->
    <?php
    if (isset($_GET['error']) && $_GET['error'] == 1) {
        echo '<p style="color: red;">Username atau password salah!</p>';
    }
    ?>
    <form action="" method="post">
        <label for="username">Username : </label>
        <input type="text" id="username" name="username" required><br>

        <label for="password">Password : </label>
        <input type="password" id="password" name="password" required><br>

        <input type="submit" value="Login">
    </form>
</center>
</body>
</html>
