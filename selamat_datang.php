<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link type="icon" rel="icon" href="img/icon/favicon.ico" />
    <title>UAS</title>
    
    <!-- ambil CSS lokal -->
    <link rel="stylesheet" href="style.css" />
    <script src="script.js"></script>
</head>

<body>
    <!-- header dan menu -->
    <header>
        <a class="brand">BIODATA DIRI MAHASISWA</a>
        <div></div>
        <div class="navigasi" style="font-weight: bold">
            <strong>
                <a href="#home">Home</a>
                <a href="#about">About</a>
                <!-- <a href="#project">Project</a> -->
                <a href="#kontak">Kontak</a>        
            </strong>
        </div>
    </header>
    <!-- akhir header -->

    <!-- home -->
    <section id="home" class="home" style="">
        <div class="content1">
            <center><div class="animasi-text">
                <h3>Konnichiwa minaa</h3>
                <h3>Informatika</h3>
                <h3>Universitas Ahmad Dahlan</h3>
               
            </div></center>
            <center>
            <!-- Tambahkan event listener pada tombol "Project Website" -->
            <div style="padding-top: 50px">
                <a class="btns" href="#project" onclick="showMessage()">Project Website</a>
            </div>
        </center>
    </div>
    <div class="img-card">
        <img src="fotoku.jpg" class="side-image" alt="Side Image">
    </div>
        </div>
    </section>
    <!-- akhir  home -->

    <!-- awal about -->
    <section class="about" id="about">
        <div class="title">
            <h2 class="section-title">About Me</h2>
            <hr style="width: 300px; height: 5px; border-radius: 10px" />
        </div>
        <div class="content">
           <center> <div class="col-left">
                <div class="img-card">
                    <img src="fotoku.jpg" alt="" />
                </div>
            </div>
            <div class="col-right">
                <!-- paragraf 1 -->
                <p style="text-align: justify">

                    <p>Hallo, namaku Siti nur rohmah biasa di panggil Rahma. aku seorang pelajar di universitas Ahmad Dahlan Yogyakarta. Lahir di kota Magetan,Jawa Timur. aku mempunyai hoby travelling untuk mencari suasana baru dan untuk menenangkan hati dan pikiran. Selain hoby, aku juga tertarik untuk mencoba hal baru seperti mengikuti banyak kepanitiaan yang di adakan oleh kampus maupun prodi.aku juga tertarik dengan dunia IT karena teknologi modern akan selalu bergerak maju dan juga dunia per-IT an hampir dibutuhkan di semua pekerjaan. aku juga menyukai film yang seru dan tidak membosankan heheee.</p><br>
                <p>hello, my name is Siti nur rohmah, usually called Rahma. I'm a student at Ahmad Dahlan University, Yogyakarta. Born in the city of Magetan, East Java. I have a hobby of traveling to find a new atmosphere and to calm my heart and mind. Apart from hobbies, I am also interested in trying new things such as participating in many committees held by campuses and study programs. I am also interested in the world of IT because modern technology will always move forward and also the world of IT is almost needed in all jobs. I also like movies that are fun and not boring hehe</p>
                    
                </p><br>
                <br>
                <!-- tabel 1 untuk data pendidikan -->
                <table border="1">
                    <tr>
                        <td colspan="2" align="center">
                       <h4>Riwayat Pendidikan</h4>
                   </td>
               </tr>
           <tr>
        <td>
          <!-- list 1: pendidikan -->
        <ul>
        <li>SDN Manisrejo</li>
        <li>SMPN 1 Karangrejo</li>
        <li>SMK Penerbangan Angkasa</li>
      </ul>
         </td>
             </tr>
                </table>
                <br>
                <!-- tabel 2 untuk data perkerjaan -->
                <table border="1">
                    <tr>
                        <td colspan="2" align="center">
                            <h4>Riwayat Kepanitiaan</h4>
                        </td>
                    </tr>
                    <tr>
                        <th>Tahun </th>
                        <th>Kepanitiaan</th>
                    </tr>
                    <tr>
                        <td>
                            <!-- list 2: pekerjaan -->
                            <ul>
                                <li>2023</li>
                            </ul>
                        </td>
                        <td>
                            <!-- list 2: pekerjaan -->
                            <ul>
                                <li> <em>Pekan Ramadhan Informatika (PRAKTIK)</em></li>
                                <li> <em>FTI Mengabdi</em></li>
                                <li> <em>IT Olympic</em></li>
                                <li> <em>Next Gen Entrepreneurship Competiion (NGEC)</em></li>
                                <li> <em>Workshop</em></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <!-- akhir about -->

    <!-- awal project -->
    <section id="project" class="project" style="background-image: url(img/bgmain.png)">
        <div class="title">
            <h2 class="section-title">Gambar Project Website Saya</h2>
            <hr style="width: 500px; height: 5px; border-radius: 10px; color: aliceblue" />
        </div><br>
        <p>Saya membuat website tentang pengiriman jasa yang dimana kita harus menginputkan nama customer,alamat customer,berapa KM untuk pengirimannya sehingga dapat menyimpan data pengiriman.Setelah menginputkan kita bisa melihat hasil datanya</p>
        <div class="content">
            <div class="card">
                <div class="img-card"><a href="http://localhost/responsi%20prak%20pweb/">
                    <img src="project.png" /></a>
                </div>
            </div>
        </section>

    <!-- awal kontak -->
    <section id="kontak" class="kontak"  style="background-image: url(img/bgmain.png)">
        <div class="title">
            <h2 class="section-title">Kontak Saya</h2>
            <hr style="width: 500px; height: 5px; border-radius: 10px" />
        </div>
        <div class="content">

            <div class="card">
                <div class="img-card">
                    <p>TIKTOK</p>
                    <br><a href="https://www.tiktok.com/@userrrrrrrr2384?_t=8e9OHBDFVsv&_r=1" target="blank" />
                    <img src="tiktok.png" /></a>
                </div>
            </div>
            <div class="card">
                <p>INSTAGRAM</p>
                <div class="img-card"><a href="https://instagram.com/cldrahma_?igshid=ZDc4ODBmNjlmNQ==" target="blank" />
                    <img src="ig.ico" /></a></div>
            </div>
            <div class="card">
                <p>G-MAIL</p>
                <div class="img-card"><a href="mailto:sitirahma45678@gmail.com" target="blank" />
                    <img src="gm.jpg" /></a></div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <p>&copy; 2023 TA UAS pweb - [siti nur rohmah/2200018296]</p>
    </footer>
    <!-- akhir footer -->

</body>

</html>